module golang-test

go 1.14

require (
	github.com/gorilla/mux v1.7.0
	gopkg.in/yaml.v2 v2.3.0
)
