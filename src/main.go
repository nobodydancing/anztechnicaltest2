package main

import (
	"fmt"
	"log"
    "net/http"
	"time"
    "os"

    "github.com/gorilla/mux"

    "gopkg.in/yaml.v2"
    "encoding/json"
)

// Config struct for webapp config
type Config struct {
    Server struct {
        Host string `yaml:"host"`

        Port    string `yaml:"port"`
        Timeout struct {

            Write time.Duration `yaml:"write"`

            Read time.Duration `yaml:"read"`

        } `yaml:"timeout"`

        Version string `yaml:"version"`
            
        Description string `yaml:"description"`

    } `yaml:"server"`
}

// Version response struct
type Version struct {
    Version string `json:"version"`
    LastCommitSHA string `json:"lastcommitsha"`
    Description string `json:"description"`
}

type versionHandler struct {
    Version string
    Description string
    LastCommitSHA string
}

// Variable to store /version response
var Version_resp Version

// Variable to store Commit hash - This variable will be injected at build
var GitCommit string

// Need the versionHandler to encapsulate variables
func (v_resp *versionHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
    

    Version_resp = Version{Version:v_resp.Version, LastCommitSHA:v_resp.LastCommitSHA, Description:v_resp.Description}
    js, err := json.Marshal(Version_resp)
    if err != nil {
        log.Fatal(err)
    }
	w.Write(js)

}

func main() {

    config, err := ReadConfig();
    if err != nil {
        log.Fatal(err)
    }
    
    // Start server

	fmt.Println("running http server...")
	r := mux.NewRouter()
	r.HandleFunc("/", homePageHandler)
    r.Handle("/version", &versionHandler{Version:config.Server.Version, Description:config.Server.Description, LastCommitSHA:GitCommit})

	s := &http.Server{
		Handler:      r,
		Addr:         config.Server.Host + ":" + config.Server.Port,
		WriteTimeout: config.Server.Timeout.Read * time.Second,
		ReadTimeout:  config.Server.Timeout.Write * time.Second,
    }
	log.Fatal(s.ListenAndServe())
}

func ReadConfig() (*Config, error){
    
    // Read config

    config := &Config{}

    file, err := os.Open("./config.yml")
    if err != nil {
        return nil, err
    }
    defer file.Close()

    d := yaml.NewDecoder(file)

    if err := d.Decode(&config); err != nil {
        return nil, err
    }

	fmt.Println("config read successfully...")
    return config, nil
    
}

func homePageHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Welcome to the HomePage. Please visit /version for more info")
}
