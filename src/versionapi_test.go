package main

import (
	"testing"
    "net/http"
	"net/http/httptest"
	"encoding/json"
)

func TestReadConfigFile(t *testing.T) { 
	config, err := ReadConfig();

	if err != nil {t.Errorf("Error when reading the config file. Error: '%v'", err) }

	if config.Server.Version == ""  {t.Errorf("Version not found in config.yml file") }
	
	if config.Server.Description == ""  {t.Errorf("Description not found in config.yml file") }

}

func TestVersionEndpoint(t *testing.T) {

    req, err := http.NewRequest("GET", "/version", nil)
	if err != nil {
		t.Fatal(err)
	}

	// Set up some test variables
	var Version, Description, LastCommitSHA string
	Version = "1.0"
	Description = "Test Description"
	LastCommitSHA = "Test Hash"

    response := executeVersionRequest(req, Version, Description, LastCommitSHA)
    checkResponseCode(t, http.StatusOK, response.Code)
	//fmt.Println(response.Body)
	
    var m map[string]interface{}
    json.Unmarshal(response.Body.Bytes(), &m)

    if m["version"] != Version {
        t.Errorf("Response returned wrong version: Got %v Expected %v", m["version"], Version )
    }

    if m["description"] == nil {
        t.Errorf("Response returned wrong description: Got %v Expected %v", m["description"], Description )
    }

	if m["lastcommitsha"] == nil {
        t.Errorf("Response returned wrong lastcommitsha: Got %v Expected %v", m["lastcommitsha"], LastCommitSHA )
    }
}

func executeVersionRequest(req *http.Request, version string, description string, lastcommitSHA string) *httptest.ResponseRecorder {
	rr := httptest.NewRecorder()

	handler := http.Handler(&versionHandler{Version:version, Description:description, LastCommitSHA:lastcommitSHA})
	handler.ServeHTTP(rr, req)

    return rr
}

func checkResponseCode(t *testing.T, expected, actual int) {
	if expected != actual {
        t.Errorf("Handler returned wrong status code: got %v want %v",
		actual, expected)
    }
}