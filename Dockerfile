FROM golang:1.14 AS builder

ENV GO111MODULE=on

WORKDIR /app/src

ADD ./ /app

# Go mod download get dependancies - will also be cached if we won't change mod/sum
RUN go mod download


# Added CGO_ENABLED=0 GOOS=linux GOARCH=amd64 to make sure the binary can be run from any OS
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags "-X main.GitCommit=$(git rev-parse HEAD)" -o golang-webapi  .

FROM scratch AS final

WORKDIR /app/src

COPY --from=builder /app /app

ENTRYPOINT ["/app/src/golang-webapi"]

EXPOSE 8000
