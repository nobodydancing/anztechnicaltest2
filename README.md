# This is a submission for ANZ technical Test 2

# Notes

* A web api has been built using golang and unit tests are created for the ReadConfig function and the handler for the /version endpoint
* A CI pipeline with build and test stages has been created on Gitlab for the repo, whenever a commit/push happens the pipeline will be triggered automatically
* The output of the pipeline is a binary file of the golang program
* The LastCommitSHA is injected in the go build command and it should return with when browsed to or GET the /version endpoint
* I ran out of time trying to 
    - properly version the builds and have it returned within the /version response
    - containerize all dependencies and include the deploy stage in the CI pipeline
    - include any security related process
    - Include a dev branch to represent development code and include a merging strategy into master

# Instructions

* The tested (not ideal) way is to download the source code folder and run the endpoint locally. This uses the docker file to build and run it. e.g.

    (While in the folder with the dockerfile)

    docker build -t anztest2:1.0 .

    docker run -p 8001:8001 anztest2:1.0

    From the host machine, browse to "http://127.0.0.1:8001/version" or use postman to GET the same endpoint

* (Untested) To run the (artifact) binary file created by gitlab from the CI pipeline you will need to download the src/config.yml and put it in the same folder as the binary file

